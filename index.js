var APIConfig = require('./config/api_urls');
var ajax = require('./node_modules/ajax-request/index');
var Listener = require('./config/listeners');

global.APIConfig = APIConfig;
global.ajax = ajax;
global.Listener = Listener;

module.exports =  {
    Shows : require('./models/show'),
    Episodes : require('./models/episode') ,
    People : require('./models/people')
};