var assert = require('assert');
var tvseries = require('../index');

describe('Shows Tests', function() {

    /** SHOWS LIST TESTS **/
    it('Request shows list', function(done) {
        tvseries.Shows.find('walking dead', function(list){
            assert.notEqual(list, null);
            assert.notEqual(list.length, 0);
            done();
        });
    });

    /** SPECIFIC SHOW TESTS **/
    it('Request not existing show', function(done) {
        tvseries.Shows.show(0, function(show){
            assert.notEqual(show, null);
            assert.equal(show.status, 404);
            done();
        });
    });



    it('Request specific show', function(done) {
        tvseries.Shows.show(73, function(show){
            assert.notEqual(show, null);
            assert.equal(show.id, 73);
            done();
        });
    });


});