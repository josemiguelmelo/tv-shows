var assert = require('assert');
var tvseries = require('../index');

describe('People Tests', function() {

    it('Find people', function(done) {
        tvseries.People.find("lauren", function(list){
            assert.notEqual(list, null);
            assert.notEqual(list.length, 0);
            done();
        });
    });

    it('Show person', function(done) {
        tvseries.People.show(1, function(person){
            assert.notEqual(person, null);
            assert.equal(person.id, 1);
            done();
        });
    });





});