var assert = require('assert');
var tvseries = require('../index');

describe('Show Episodes Tests', function() {

    it('TV Show check existence', function(done) {
        tvseries.Shows.show(73, function(show){
            assert.notEqual(show, null);
            assert.equal(show.id, 73);
            done();
        });
    });

    it('TV Show Episodes', function(done) {
        tvseries.Episodes.allShowEpisodes(73, function(episodes){
            assert.notEqual(episodes, null);
            assert.notEqual(episodes.length, 0);
            done();
        });
    });

    it('Specific Episode', function(done){
       tvseries.Episodes.show(1, function(episode){
           assert.notEqual(episode, null);
           assert.equal(episode.id, 1);
           done();
       }) ;
    });





});