
module.exports = {
    differenceInDays : function(date1, date2)
    {
        var oneDay = 24*60*60*1000;
        return Math.round((date1 - date2)
                / (oneDay) ) + 1;
    }
};