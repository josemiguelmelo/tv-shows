# tv-shows

Package to get information about TV shows - general information, shows, casts, ...
This takes advantage of an existing API - _http://www.tvmaze.com/api_


## Install
Run on command line

`npm install tv-series --save`

**OR**

Add to package.json

`"tv-shows" : "1.1.0"`

and run on command line

`npm install`


## Test

``mocha``

## Configuration

All configuration files are located at _config/_

### Listeners

To configure listeners, which are used to listen to specific episodes/shows, just open _config/listeners.js_

**Timer**
Frequency to check for changes.

**Days Before**
Number of days before event happens. Used to send notification to the system/call callback function.


## Methods

There are 3 types of objects, which contain methods.

### Shows

**find(query, callback)**

- `query` required
    - string with the name of the tv show to search for
- `callback` required
    - receives the list with all the results


    ```
    find('walking dead', function(resultsList){

    });
    ```

**show(showID, callback)**

- `showID` required
    - id of the TV Show
- `callback` required
    - receive the TV Show with specified id

    ```
    show(1, function(tvShow){

    });
    ```
    
**listen(showID, callback)**

This method allows you to listen to a specific show. When next episode is on the next day, it calls the callback function.


- `showID` required
    - id of the TV Show
- `callback` required
    - receive the next episode

    ```
    listen(1, function(episode){

    });
    ```
    

### Episodes   

**allShowEpisodes(showID, callback)**

- `showID` required
    - id of the TV Show
- `callback` required
    - receives a list with all episodes for the TV Show with specified id

    ```
    allShowEpisodes(1, function(episodesList){

    });
    ```
    
**show(episodeID, callback)**

- `episodeID` required
    - id of the episode
- `callback` required
    - receives the episode with specified ID

    ```
    show(1, function(episode){

    });
    ```
    


### People


**find(query, callback)**

- `query` required
    - string with the name of the person to search for
- `callback` required
    - receives a list with all people with name specified

    ```
    find("lauren", function(peopleList){

    });    
    ```


**show(personID, callback)**

- `personID` required
    - id of the person
- `callback` required
    - receive the person with the ID specified

    ```
    show(1, function(person){

    });
    ```


## Example Usage

This example is using Express.

**routes/index.js**

```
    var express = require('express');
    var router = express.Router();

    var tvshows = require('tv-shows');


    /* GET tv shows listing with name 'walking dead'. */
    router.get('/shows', function(req, res, next) {

      tvshows.Shows.find("walking dead", function(list){
        res.send(list);
      });

    });

    /* GET episodes for show with ID = 1 */
    router.get('/episodes', function(req, res, next) {

      tvshows.Episodes.show(1, function(list){
        res.send(list);
      });

    });


    /* GET people listing with name 'lauren' */
    router.get('/people', function(req, res, next) {

      tvshows.People.find("lauren", function(list){
        res.send(list);
      });

    });

    module.exports = router;
```
