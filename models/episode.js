
module.exports = {

    allShowEpisodes : function(showId, callback)
    {
        global.ajax({
            url: global.APIConfig.show_episodes_url.replace('{showID}', showId),
            method: 'GET'
        }, function(err, res, body) {
            var episodesList = JSON.parse(body);
            callback(episodesList);
        });
    },

    show : function(episodeID, callback)
    {
        global.ajax({
            url: global.APIConfig.episode_url.replace('{episodeID}', episodeID),
            method: 'GET'
        }, function(err, res, body) {
            var episode = JSON.parse(body);
            callback(episode);
        });
    }

};