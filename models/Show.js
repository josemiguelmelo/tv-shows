var Episodes = require('./episode');
var DatesHelper = require('../helper/date');

module.exports =  {
    find : function(query, callback)
    {
        global.ajax({
            url: global.APIConfig.search_url,
            method: 'GET',
            data: {
                q: query
            }
        },function(err, res, body) {
            var showsList = JSON.parse(body);
            callback(showsList);
        });
    },

    show : function(showId, callback)
    {
        global.ajax({
            url: global.APIConfig.show_url.replace('{showID}', showId),
            method: 'GET'
        },function(err, res, body) {
            var show = JSON.parse(body);
            callback(show);
        });
    },

    listen : function (showId, callback) {

        this.show(showId, function(show){

            if(show._links.hasOwnProperty('nextepisode'))
            {
                var epURL = global.APIConfig.episode_url.replace('{episodeID}', '');
                var episodeID = show._links.nextepisode.href.replace(epURL, '');

                Episodes.show(episodeID, function(episode){
                    var airDate = new Date(episode.airdate);
                    var todayDate = new Date();
                    var differenceDates = DatesHelper.differenceInDays(airDate, todayDate);

                    if(differenceDates == global.Listener.days_before)
                    {
                        callback(episode);
                    }else if(differenceDates > global.Listener.days_before){
                        var timer = setInterval(function() {
                            clearInterval(timer);
                            module.exports.listen(showId, callback);
                        }, global.Listener.timer);
                    }
                });
            }
        });
    }
};