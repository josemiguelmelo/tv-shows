

module.exports = {

    find : function(query, callback)
    {
        global.ajax({
            url: global.APIConfig.search_people_url,
            method: 'GET',
            data: {
                q: query
            }
        }, function(err, res, body) {
            var peopleList = JSON.parse(body);
            callback(peopleList);
        });
    },

    show : function(personId, callback){
        global.ajax({
            url: global.APIConfig.show_person_url.replace('{personID}', personId),
            method: 'GET'
        }, function(err, res, body) {
            var person = JSON.parse(body);
            callback(person);
        });
    }
};