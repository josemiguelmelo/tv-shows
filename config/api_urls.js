var baseURL = 'http://api.tvmaze.com';

module.exports = {
  'search_url' : baseURL + '/search/shows',
  'show_url' : baseURL + '/shows/{showID}',
  'show_episodes_url' : baseURL + '/shows/{showID}/episodes',
  'search_people_url' : baseURL + '/search/people',
  'show_person_url' : baseURL + '/people/{personID}',
  'episode_url' : baseURL + '/episodes/{episodeID}'

};